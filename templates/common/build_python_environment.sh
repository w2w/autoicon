# Get some variables provided by autosubmit.
# TODO: What do we do to ensure that these variables are defined in the proj file?
WORKDIR=%HPCROOTDIR%
ICON_VERSION=%ICON_VERSION%
SPACK_URL=%SPACK.URL%
SPACK_BRANCH=%SPACK.BRANCH%
SPACK_COMPILER=%SPACK.COMPILER%

PYTHON_VERSION=%PYTHON_ENVIRONMENT.PYTHON_VERSION%

MODULES="%MODULES%"
if [ ! -z "${MODULES}" ]; then module load ${MODULES}; fi

# Go to the working directory
cd ${WORKDIR}

. ${WORKDIR}/proj/platforms/common/spack_utils.sh
load_spack "%spack.init%" "%spack.root%" "%spack.url%" "%spack.branch%" "%spack.externals%" "%spack.compiler%" "%spack.disable_local_config%" "%spack.user_cache_path%" "%spack.user_config_path%" "%spack.upstreams%"



# Use spack to get a recent enough version of python3
if [ $(spack find python@${PYTHON_VERSION}: &>/dev/null ) ]; then
  echo "python@${PYTHON_VERSION} already installed!"
else
  echo "Installing a version of python3"
  # Compile openmpi with schedulers=slurm
  spack install python@${PYTHON_VERSION}
fi

# Load the python module
spack load --first python@${PYTHON_VERSION}

PYTHON_ENVIRONMENT_FOLDER=%python_environment.folder_name%

if [ ! -d ${PYTHON_ENVIRONMENT_FOLDER} ]; then
  # Create virtual environment
  python3 -m venv --prompt AS ${PYTHON_ENVIRONMENT_FOLDER}
fi

# Load environment
source ${PYTHON_ENVIRONMENT_FOLDER}/bin/activate

# Create a link to the binary
ln -sf $(which python3) ${WORKDIR}/python3

# Install the requirements via pip
# TODO: Due to a incompatibility issue between latest numba and numpy I have to add this here. Hopefully removable soon.
requirements="%python_environment.requirements%"

# Convert list with python format to a bash array
requirements=($(echo ${requirements} | sed "s/'//g" | tr -d '[],'))

#TODO: Shouldn't be necessary but it is for now to overcome an issue with a repetition of the requirements.
# Use sort and uniq to get the unique elements
unique_requirements=($(printf "%s\n" "${requirements[@]}" | sort -u))

# Print the unique elements
echo "${unique_requirements[@]}"

# Install requirements.
for requirement in "${unique_requirements[@]}"; do
  python -m pip install ${requirement}
done

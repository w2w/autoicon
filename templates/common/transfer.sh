# Move the outputs to the proper destination

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
STARTDATE=%SDATE%
MEMBER=%MEMBER%
OUTPUT_FILES="%SIMULATION.OUTPUT_FILE_NAMES%"
HPCUSER=%HPCUSER%
HPCHOST=%HPCHOST%

# Define output dir in remote machine
OUTPUT_DIR=${WORKDIR}/output/${STARTDATE}/${MEMBER}

MAIN_LOCAL_FOLDER=%data_management.local_destination_folder%/%DEFAULT.EXPID%

DESTINATION_DIR=${MAIN_LOCAL_FOLDER}/${STARTDATE}/${MEMBER}

mkdir -p ${DESTINATION_DIR}

if [[ "x$HPCHOST" == "xlocalhost" ]]; then
  for file_name in ${OUTPUT_FILES}; do
    rsync -v ${OUTPUT_DIR}/${file_name} ${DESTINATION_DIR}
  done
else
  # Copy the output files
  for file_name in ${OUTPUT_FILES}; do
    rsync -v ${HPCUSER}@${HPCHOST}:${OUTPUT_DIR}/${file_name} ${DESTINATION_DIR}
  done
fi

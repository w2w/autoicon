# Synchronize the local namelists with the remote directory

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
HPCUSER=%HPCUSER%
HPCHOST=%HPCHOST%

# Define local and remote namelists folders
REMOTE_WORKDIR=${WORKDIR}/
PROJ_FOLDER="%PROJDIR%"

# Transfer the project
if [[ "x$HPCHOST" == "xlocalhost" ]]; then
  mkdir -p ${REMOTE_WORKDIR}/proj
  rsync -v -u -r --no-relative ${PROJ_FOLDER}/ ${REMOTE_WORKDIR}/proj
else
  ssh ${HPCUSER}@${HPCHOST} mkdir -p ${REMOTE_WORKDIR}/proj
  rsync -v -u -r --no-relative ${PROJ_FOLDER}/ ${HPCUSER}@${HPCHOST}:${REMOTE_WORKDIR}/proj
fi

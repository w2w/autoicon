#!/bin/bash -l

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
STARTDATE=%SDATE%
MEMBER=%MEMBER%

# Common folder with data needed for all simulations
COMMON_INIDATA_FOLDER=${WORKDIR}/inidata

# Member folder
MEMBER_DIR=${WORKDIR}/${STARTDATE}/${MEMBER}

# Create member folder and go there
mkdir -p ${MEMBER_DIR}

cd ${MEMBER_DIR} || exit


# Link all files from the common inidata folder and the common date folder
ln -sf ${COMMON_INIDATA_FOLDER}/* .

# Link files from the ideal run.
ln -sf ../ideal/extpar_DOM01.nc .
ln -sf ../ideal/init-test-fg_DOM01_ML_0001.nc
ln -sf ../ideal/init-test-ana_DOM01_ML_0001.nc

#!/bin/bash -l

# Get some variables provided by autosubmit.
WORKDIR=%HPCROOTDIR%
STARTDATE=%SDATE%

# Common folder with data needed for all simulations
COMMON_INIDATA_FOLDER=${WORKDIR}/inidata

# Ideal folder
IDEAL_DIR=${WORKDIR}/${STARTDATE}/ideal

# Create member folder and go there
mkdir -p ${IDEAL_DIR}
cd ${IDEAL_DIR} || exit

# Link all files from the common inidata folder and the common date folder
ln -sf ${COMMON_INIDATA_FOLDER}/* .

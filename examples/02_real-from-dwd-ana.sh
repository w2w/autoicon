#!/bin/bash
#######################################################################################################################
# Variables that are required to define the test case
EXPID="rfda"
ICON_CASE="real-from-dwd-ana"
ICON_VERSION="2.6.5-nwp0"
PLATFORM="LOCAL"
EXTRA_NAMELIST=""
EXTRA_CONFIGURATION=""

#######################################################################################################################
set -euo pipefail

# Get path to the script directory
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Import the function run_autoicon_example
source ${SCRIPT_DIR}/example_utils.sh

# Setup and run the experiment
run_autoicon_example "$EXPID" "$ICON_CASE" "$ICON_VERSION" "$PLATFORM" "${EXTRA_NAMELIST}" "${EXTRA_CONFIGURATION}"

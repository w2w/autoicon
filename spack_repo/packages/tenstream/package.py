# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *

class Tenstream(CMakePackage):
    """
    Recipe to build TenStream radiative transfer lib.
    """

    homepage = "https://gitlab.com/jakubfabian/tenstream.git"

    git = "https://gitlab.com/jakubfabian/tenstream.git"

    version("master", branch="master")
    version("next", branch="next")

    variant("debug", default=False, description="use debug flags to build")
    variant("rayli", default=False, description="download and build with RayLI - MonteCarlo raytracer for unstructured meshes")

    # Dependencies
    depends_on("mpi")
    depends_on("cmake")
    depends_on("netcdf-fortran")
    depends_on("petsc")

    def setup_build_environment(self, env):
        spec = self.spec

        # Some environment variables to set
        env_variables_to_set = {
            "CC": spec["mpi"].mpicc,
            "CXX": spec["mpi"].mpicxx,
            "FC": spec["mpi"].mpifc,
            "F77": spec["mpi"].mpif77,
        }
        for variable, value in env_variables_to_set.items():
                env.set(variable, value)

    def cmake_args(self):
        args = []

        if "+debug" in self.spec:
            args.append('-DCMAKE_BUILD_TYPE=DEBUG')

        if "+rayli" in self.spec:
            args.append("-DBUILD_RAYLI=ON")

        return args
